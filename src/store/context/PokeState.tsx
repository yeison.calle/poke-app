import { useMemo, useReducer, FC } from 'react'
import PokeContext from './PokeContext'
import { getPokemonListAction } from '../actions/pokeActions'
import pokeReducer from '../reducers/pokeReducer'

const PokeState: FC = ({ children }) => {
  const initialState = {
    pokeList: [],
    isLoading: false,
    isError: false
  }

  const [state, dispatch] = useReducer(pokeReducer, initialState)

  const getPokemonList = ({ limit, offset }: any = {}) =>
    getPokemonListAction({ limit, offset })(dispatch)

  const pokeContextValue = useMemo(
    () => ({
      pokeList: state.pokeList,
      isLoading: state.isLoading,
      isError: state.isError,
      getPokemonList
    }),
    [state]
  )

  return (
    <PokeContext.Provider value={pokeContextValue}>
      {children}
    </PokeContext.Provider>
  )
}

export default PokeState
