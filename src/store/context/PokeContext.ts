import { createContext } from 'react'

interface pokeContextProps {
  pokeList: any[];
  getPokemonList: Function;
  isLoading: boolean;
  isError: boolean;
}

const defaultState = {
  pokeList: [],
  isLoading: false,
  isError: false,
  getPokemonList: () => {}
}

const PokeContext = createContext<pokeContextProps>(defaultState)

PokeContext.displayName = 'Pokemon Context'

export default PokeContext
