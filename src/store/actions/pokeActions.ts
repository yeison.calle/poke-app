import { POKE_ENDPOINT } from '../../shared/constants/appConstants'
import { IStepsProps } from '../../shared/interfaces/IStepProps'
import serviceHandler from '../../shared/services/serviceHandler'
import {
  GET_POKE_ERROR,
  GET_POKE_START,
  GET_POKE_SUCCESS
} from './types/pokeTypes'

const getPokemonDetails = async (data: any) => {
  const payloadRequests = await data.results.map(async (pokemon: any) => {
    const request = await fetch(pokemon.url)
    return request.json()
  })

  return Promise.all(payloadRequests)
}

const pokeSteps: IStepsProps = {
  start: () => ({
    type: GET_POKE_START
  }),
  success: async (data: any) => {
    const pokemonDetails = await getPokemonDetails(data)

    return {
      type: GET_POKE_SUCCESS,
      payload: pokemonDetails
    }
  },
  error: (err: any) => ({
    type: GET_POKE_ERROR,
    payload: err
  })
}

export const getPokemonListAction =
  ({ limit = 0, offset = 0 }: any) =>
    async (dispatch: Function) => {
      await serviceHandler({
        url: `${POKE_ENDPOINT}?limit=${limit}&offset=${offset}`,
        steps: pokeSteps,
        dispatch
      })
    }
