import {
  GET_POKE_START,
  GET_POKE_SUCCESS,
  GET_POKE_ERROR
} from '../actions/types/pokeTypes'

const initialState = {
  pokeList: [],
  isLoading: false,
  isError: false
}

const pokeReducer = (state: any = initialState, { type, payload }: any) => {
  const stateReducer: any = {
    [GET_POKE_START]: {
      ...state,
      isLoading: true
    },
    [GET_POKE_SUCCESS]: {
      ...state,
      pokeList: payload,
      isLoading: false,
      isError: false
    },
    [GET_POKE_ERROR]: {
      ...state,
      isLoading: false,
      isError: true
    }
  }

  return stateReducer[type] || state
}

export default pokeReducer
