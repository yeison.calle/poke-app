import './loader.scss'

const Loader = () => (
  <div className="loader__wrapper">
    <div className="loader" />
  </div>
)

export default Loader
