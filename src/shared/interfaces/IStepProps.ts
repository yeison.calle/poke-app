export interface IStepsProps {
  start: Function;
  success: Function;
  error: Function;
}
