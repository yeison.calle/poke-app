export const POST_METHOD = 'POST'
export const GET_METHOD = 'GET'
export const POKE_ENDPOINT = 'https://pokeapi.co/api/v2/pokemon'
export const HTTP_SUCCESS_CODE = 200
