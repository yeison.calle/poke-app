import axios from 'axios'
import { GET_METHOD, HTTP_SUCCESS_CODE, POST_METHOD } from '../constants/appConstants'
import { IStepsProps } from '../interfaces/IStepProps'

interface serviceHandlerProps {
  url: string;
  method?: string;
  steps: IStepsProps;
  dispatch: Function;
  options?: object;
}

export const requestByMethod = (method: string) => {
  const requests: any = {
    [GET_METHOD]: axios.get,
    [POST_METHOD]: axios.post
  }

  return requests[method] || requests[GET_METHOD]
}

const serviceHandler = async ({
  url,
  method,
  steps,
  dispatch,
  options
}: serviceHandlerProps) => {
  try {
    dispatch(steps.start())

    const response = await requestByMethod(method || GET_METHOD)(url, options)

    if (response.status !== HTTP_SUCCESS_CODE) throw new Error(response.statusText)

    dispatch(await steps.success(response.data))
  } catch (error) {
    dispatch(steps.error(error))
  }
}

export default serviceHandler
