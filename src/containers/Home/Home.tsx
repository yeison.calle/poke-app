import { FormEvent, useContext, useEffect, useState, FC } from 'react'
import { Link, useSearchParams } from 'react-router-dom'
import { FiChevronRight, FiChevronLeft } from 'react-icons/fi'
import PokeContext from '../../store/context/PokeContext'

import './home.scss'
import Loader from '../../components/Loader/Loader'

const PAGINATION_LIMIT: number = 20
const INITIAL_OFFSET: number = 0

const Home: FC = () => {
  const [searchParams, setSearchParam] = useSearchParams()

  const pageParam = searchParams.get('page')
  const pokemonPage = pageParam ?? 1
  const pageOffset = (Number(pokemonPage) - 1) * PAGINATION_LIMIT
  const initialOffset = pageParam ? pageOffset : INITIAL_OFFSET

  const [filterText, setFilterText] = useState('')
  const [paginationOffset, setPaginationOffset] = useState(initialOffset)

  const isFirstPage: boolean = paginationOffset === INITIAL_OFFSET &&
    (pageParam === '1' || !pageParam)

  const { pokeList, isLoading, getPokemonList } = useContext(PokeContext)

  useEffect(() => {
    getPokemonList({
      limit: PAGINATION_LIMIT,
      offset: pageParam ? pageOffset : paginationOffset
    })
  }, [paginationOffset, pageParam])

  const filteredItems = pokeList.filter((item: any) =>
    item.species.name?.toLowerCase()?.includes(filterText.toLowerCase())
  )

  const handleFilterChange = (e: FormEvent<HTMLInputElement>) => {
    setFilterText(e.currentTarget.value)
  }

  const setPageParam = (offset: number): void => {
    const page = (offset / PAGINATION_LIMIT + 1).toString()
    setSearchParam({ page })
  }

  useEffect(() => {
    setPageParam(paginationOffset)
  }, [paginationOffset])

  const handlePrevious = () => {
    setPaginationOffset((po: number) => po - PAGINATION_LIMIT)
  }

  const handleNext = () => {
    setPaginationOffset((po: number) => po + PAGINATION_LIMIT)
  }

  const renderPokemonList = () => (
    <ul className="pokelist">
      {filteredItems.map(row => (
        <li key={row.id}>
          <Link to={`/${row.id}`}>
            <img
              src={row.sprites.other['official-artwork'].front_default}
              alt={row.species.name}
            />
            <span>{row.species.name}</span>
          </Link>
        </li>
      ))}
    </ul>
  )

  return (
    <section>
      <h1 className="title">Pokemon List</h1>
      <input
        className="search-input"
        disabled={isLoading}
        onChange={handleFilterChange}
        placeholder="Filter pokemon by name"
        type="search"
        value={filterText}
      />

      {isLoading && <Loader />}

      {!isLoading && renderPokemonList()}

      <div className="pagination-buttons">
        <button
          disabled={isFirstPage || isLoading}
          onClick={handlePrevious}
        >
          <FiChevronLeft />
        </button>
        <span className='pagination-buttons__count'>Page {pokemonPage}</span>
        <button
          disabled={isLoading}
          onClick={handleNext}
        >
          <FiChevronRight />
        </button>
      </div>
    </section>
  )
}

export default Home
