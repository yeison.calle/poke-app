import { useEffect, useContext, FC } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import PokeContext from '../../store/context/PokeContext'

const Details: FC = () => {
  const navigate = useNavigate()
  const { pokeId } = useParams()
  const { pokeList, isLoading, getPokemonList } = useContext(PokeContext)

  const selectedPokemon: any = pokeList.find(
    row => row.id.toString() === pokeId
  )

  useEffect(() => {
    window.scrollTo(0, 0)

    if (pokeList.length > 0) return

    getPokemonList({
      limit: pokeId
    })
  }, [])

  const goBack = () => {
    navigate(-1)
  }

  if (isLoading || !selectedPokemon) return <p>Loading...</p>

  return (
    <div>
      <img
        src={selectedPokemon.sprites.other['official-artwork'].front_default}
        alt={selectedPokemon.species.name}
      />
      <h1>{selectedPokemon.species.name}</h1>

      <button onClick={goBack} type="button">
        Regresar
      </button>
      <pre>{JSON.stringify(selectedPokemon, null, 2)}</pre>
    </div>
  )
}

export default Details
