import { BrowserRouter, Route, Routes } from 'react-router-dom'
import PokeState from '../../store/context/PokeState'
import Details from '../Details/Details'
import Home from '../Home/Home'

function App () {
  return (
    <BrowserRouter>
      <PokeState>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path=":pokeId" element={<Details />} />
        </Routes>
      </PokeState>
    </BrowserRouter>
  )
}

export default App
